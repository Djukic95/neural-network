import numpy as np


@np.vectorize
def sigmoid(x):
    return 1 / (1 + np.e ** -x)


class SimpleNN:
    def __init__(self, layer_dimensions, learning_rate):
        self.input_dim = layer_dimensions[0]
        self.hidden_dim = layer_dimensions[1]
        self.output_dim = layer_dimensions[2]
        self.learning_rate = learning_rate

        self.weights_in_hidden = np.random.uniform(
            0, 1, (self.input_dim) * self.hidden_dim)

        self.weights_hidden_out = np.random.uniform(
            0, 1, self.output_dim * (self.hidden_dim))

        self.weights_in_hidden = self.weights_in_hidden.reshape(
            self.hidden_dim, self.input_dim)

        self.weights_hidden_out = self.weights_hidden_out.reshape(
            self.output_dim, self.hidden_dim)

    def train(self, input_vector, target_vector):

        input_vector = np.array(input_vector, ndmin=2).T
        target_vector = np.array(target_vector, ndmin=2).T

        hidden_layer_out = sigmoid(
            np.dot(self.weights_in_hidden, input_vector))
        out = sigmoid(np.dot(self.weights_hidden_out, hidden_layer_out))

        output_errors = target_vector - out

        error_gradients = output_errors * out * (1.0 - out)
        h_o_updates = self.learning_rate * \
            np.dot(error_gradients, hidden_layer_out.T)
        self.weights_hidden_out += h_o_updates

        hidden_errors = np.dot(self.weights_hidden_out.T, output_errors)
        error_gradients = hidden_errors * hidden_layer_out * \
            (1.0 - hidden_layer_out)
        i_h_updates = np.dot(error_gradients, input_vector.T)
        self.weights_in_hidden += self.learning_rate * i_h_updates

    def run(self, input):
        input = np.array(input).T
        hidden_layer_out = sigmoid(np.dot(self.weights_in_hidden, input))
        return sigmoid(np.dot(self.weights_hidden_out, hidden_layer_out))


def get_dataset(size):
    BOUNDS = 10.0
    points = BOUNDS * np.random.rand(size, 3) - BOUNDS / 2.0
    z = [np.cos(p[0]) * np.cos(p[1]) for p in points]
    y = [(z[i] > points[i][2]) for i in range(len(z))]
    return (points, y)


if __name__ == "__main__":
    training_set_size = 1000
    validation_set_size = 10000
    epoch_count = 15

    data, results = get_dataset(training_set_size)
    nn = SimpleNN([3, 10, 1], learning_rate=0.05)

    for i in range(epoch_count):
        for j in range(len(data)):
            nn.train(data[j], results[j])

    test_points, expected = get_dataset(validation_set_size)
    correct_count = 0

    for i in range(len(test_points)):
        res = nn.run(test_points[i])
        correct_count += int((res > .5) == expected[i])

    print("Accuracy: {}%".format(
        (correct_count / float(validation_set_size)) * 100))

    inputFirst = float(input("Unesite prvu koordinatu: " ))
    inputSecond = float(input("Unesite drugu koordinatu: "))
    inputThird = float(input("Unesite trecu kooridnatu: "))
    inputArray = [inputFirst, inputSecond, inputThird]
    print(nn.run(inputArray))
